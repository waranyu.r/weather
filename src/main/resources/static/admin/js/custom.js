function addRowOurStory() {
    rowIndex++;

    let html =  '<div class="my-4 item-what-v-offer">' +
        '<a href="#" class="minus-button w-inline-block">' +
        '<img src="/images/trash-icon-01.svg" alt="" class="plus-icon">' +
        '</a>' +
        '<input type="hidden" id="homes' + rowIndex + '.section" name="homes[' + rowIndex + '].section" value="OURSTORY">' +
        '<div class="caption">Principle #' + (rowIndex + 1) + '</div>' +
        '<label for="principle-title-' + rowIndex + '" class="label-text">Title</label>' +
        '<input type="text" maxlength="256" id="principle-title-' + rowIndex + '" class="text-field w-input" name="homes[' + rowIndex + '].title">' +
        '<label for="principle-description-' + rowIndex + '" class="label-text">Short Description</label>' +
        '<textarea maxlength="5000" id="principle-description-' + rowIndex + '" class="text-field w-input" name="homes[' + rowIndex + '].description"></textarea>' +
        '<div class="div-line full-thin"></div>' +
        '</div>';
    if ($('.item-what-v-offer').length > 0) {
        $('.item-what-v-offer:last').after(html);
    } else {
        $('.row-our-story-end').before(html);
    }
}

function uploadFile(input) {
    const url = input.value;
    const ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

    if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
        const reader = new FileReader();
        reader.onload = function (e) {
            $(input).parents().eq(3).find('.file-image').attr('src', e.target.result)
        }
        reader.readAsDataURL(input.files[0]);
        $(input).parents().eq(3).find('.file-name').val(input.files[0].name)
    } else {
        alert("Image or GIF")
    }
}

function addRowWhatVOffer() {
    rowWhatVOfferIndex++;

    let html =  '<div class="my-4 what-v-offer">' +
        '<a href="#" class="minus-button w-inline-block">' +
        '<img src="/images/trash-icon-01.svg" alt="" class="plus-icon">' +
        '</a>' +
        '<input type="hidden" id="homes' + rowWhatVOfferIndex + '.section" name="homes[' + rowWhatVOfferIndex + '].section" value="WHATVOFFER">' +
        '<div class="caption">Offer #' + (rowWhatVOfferIndex + 1) + '</div>' +
        '<label for="wvo-title-' + rowWhatVOfferIndex + '" class="label-text">Title</label>' +
        '<input type="text" maxlength="256" id="wvo-title-' + rowWhatVOfferIndex + '" class="text-field w-input" name="homes[' + rowWhatVOfferIndex + '].title">' +
        '<label for="wvo-short-description-' + rowWhatVOfferIndex + '" class="label-text">Short Description</label>' +
        '<textarea maxlength="5000" id="wvo-short-description-' + rowWhatVOfferIndex + '" class="text-field w-input" name="homes[' + rowWhatVOfferIndex + '].description"></textarea>' +
        '<div class="div-line full-thin"></div>' +
        '</div>';

    if($('.what-v-offer').length > 0) {
        $('.what-v-offer:last').after(html);
    } else {
        $(".end-of-what-v-offer").before(html);
    }
}

function addRowMeetTheRealPlayer() {
    rowMeetTheRealPlayerIndex++;

    let html =  '<div class="my-4 meet-the-real-player-div" th:each="meetTheRealPlayer, i: *{homes}">' +
        '<a href="#" class="minus-button w-inline-block">' +
        '<img src="/images/trash-icon-01.svg" alt="" class="plus-icon">' +
        '</a>' +
        '<input type="hidden" id="homes' + rowMeetTheRealPlayerIndex + '.section" name="homes[' + rowMeetTheRealPlayerIndex + '].section" value="MEETTHEREALPLAYER">' +
        '<div class="caption">Player #' + (rowMeetTheRealPlayerIndex + 1)+ '</div>' +
        '<label for="mrp-quote-' + rowMeetTheRealPlayerIndex + '" class="label-text">Quote</label>' +
        '<input id="homes' + rowMeetTheRealPlayerIndex + '.title" name="homes[' + rowMeetTheRealPlayerIndex + '].title" type="text" maxlength="256" class="text-field w-input">' +
        '<label for="mrp-name-' + rowMeetTheRealPlayerIndex + '" class="label-text">Name</label>' +
        '<input id="homes' + rowMeetTheRealPlayerIndex + '.fullname" name="homes[' + rowMeetTheRealPlayerIndex + '].fullname"  type="text" maxlength="256" class="text-field w-input">' +
        '<label for="mrp-position-' + rowMeetTheRealPlayerIndex + '" class="label-text">Position</label>' +
        '<input id="homes' + rowMeetTheRealPlayerIndex + '.jobPosition" name="homes[' + rowMeetTheRealPlayerIndex + '].jobPosition" type="text" maxlength="256" class="text-field w-input">' +
        '<label for="mrp-video-' + rowMeetTheRealPlayerIndex + '" class="label-text">Video Link</label>' +
        '<input id="homes' + rowMeetTheRealPlayerIndex + '.videoLink" name="homes[' + rowMeetTheRealPlayerIndex + '].videoLink"  type="text" maxlength="256" class="text-field w-input">' +
        '<label for="mrp-profile-image-' + rowMeetTheRealPlayerIndex + '" class="label-text">Profile Image</label>' +
        '<div class="image-upload">' +
        '<div class="image-holder full-width">' +
        '<img class="file-image"  alt="">' +
        '</div>' +
        '<div class="button-holder-4">' +
        '<div class="file is-small is-fullwidth has-name">' +
        '<label class="file-label">' +
        '<input id="multipartFilesDocument" name="multipartFilesDocument" class="file-input" type="file">' +
        '<span class="file-cta" style="height: 100%;">' +
        '<span class="file-label has-text-weight-normal">' +
        'Upload' +
        '</span>' +
        '</span>' +
        '<input id="homes' + rowMeetTheRealPlayerIndex + '.image" name="homes[' + rowMeetTheRealPlayerIndex + '].image" class="file-name has-text-weight-normal" readonly="true">' +
        '</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="div-line full-thin"></div>' +
        '</div>';

    if ($('.meet-the-real-player-div').length > 0) {
        $('.meet-the-real-player-div:last').after(html);
    } else {
        $('.row-meet-the-real-player-end').before(html);
    }
}

function addRowCategory() {
    rowIndex++;
    let listName = 'categories'; //list name in Catalog.class

    let html =  '<div class="story-container">' +
        '<div class="cat-container">' +
        '<div class="the-form-block full-width w-form">' +
        '<input name="' + listName + '[' + rowIndex + '].name" type="text" maxlength="256" class="text-field no-margin w-input">' +
        '</div>' +
        '<a href="#" class="minus-button-2 w-inline-block">' +
        '<img src="/images/trash-icon-01.svg" alt="" class="plus-icon">' +
        '</a>' +
        '</div>' +
        '</div>';
    if($('.story-container').length > 0) {
        $('.story-container:last').after(html);
    } else {
        $('.full-thin').before(html);
    }
}

function addMoreQualification() {
    rowQualification++;

    let html =  '<div class="the-list-holder row-qualification">' +
        '<div class="bullet-holder">' +
        '<a href="#" class="minus-button-2 w-inline-block">' +
        '<img src="/images/trash-icon-01.svg" alt="" class="plus-icon">' +
        '</a>' +
        '</div>' +
        '<div class="the-form-block full-width w-form">' +
        '<textarea id="job.qualifications' + rowQualification + '" name="job.qualifications[' + rowQualification + ']"  data-name="qua bullet 1" maxlength="5000" id="qua-bullet" name="qua-bullet-1" class="text-field w-input"></textarea>' +
        '</div>' +
        '</div>';
    if ($('.row-qualification').length > 0) {
        $('.row-qualification:last').after(html);
    } else {
        $('.row-qualification-end').before(html);
    }
}

function addMoreResponsibility() {
    rowResponsibility++;

    let html =  '<div class="the-list-holder row-responsibility">' +
        '<div class="bullet-holder">' +
        '<a href="#" class="minus-button-2 w-inline-block">' +
        '<img src="/images/trash-icon-01.svg" alt="" class="plus-icon">' +
        '</a>' +
        '</div>' +
        '<div class="the-form-block full-width w-form">' +
        '<textarea id="job.responsibilities' + rowResponsibility + '" name="job.responsibilities[' + rowResponsibility + ']"  data-name="qua bullet 1" maxlength="5000" id="qua-bullet" name="qua-bullet-1" class="text-field w-input"></textarea>' +
        '</div>' +
        '</div>';
    if ($('.row-responsibility').length > 0) {
        $('.row-responsibility:last').after(html);
    } else {
        $('.row-responsibility-end').before(html);
    }
}