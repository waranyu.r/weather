// get the sticky element
const stickyElm = document.querySelector('.position-sticky')

const observer = new IntersectionObserver(
    ([e]) => {
        if (e.intersectionRatio < 1) {
            $('.overlay').addClass('isSticky')
            $('.back-to-top').addClass('active')
            $('.krusri-logo').addClass('scrolled')
        }else {
            $('.overlay').removeClass('isSticky')
            $('.back-to-top').removeClass('active')
            $('.krusri-logo').removeClass('scrolled')
        }
        // e.intersectionRatio < 1 ? $('.overlay').addClass('isSticky') : $('.overlay').removeClass(
        //     'isSticky');
        // e.intersectionRatio < 1 ? $('.back-to-top').addClass('active') : $('.back-to-top').removeClass(
        //     'active');
    }, {
    threshold: [1]
});
observer.observe(stickyElm)

$('.wrap-nav-icon').click(function () {
    $(this).toggleClass('active')
    $('nav').toggleClass('active')
    $('nav').toggleClass('animate__slideInRight')
    $('body').addClass('active')
})

$('.close-btn').click(function () {
    $('.wrap-nav-icon').toggleClass('active')
    $('nav').toggleClass('active')
    $('nav').toggleClass('animate__slideInRight')
    $('body').removeClass('active')
})

$('.back-to-top').click(function () {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
})

// scrolling start //

window.addEventListener('scroll', function (e) {
    showElement(e.srcElement.scrollingElement.scrollTop);
});


function showElement(scrollVal) {
    findClassForLoop($('.content-animate'), scrollVal);
}


function findClassForLoop(selector, scroll) {
    for (let index = 0; index < selector.length; index++) {
        if (scroll > selector[index].offsetTop - 800) {
            selector.eq(index).addClass('animate');
            // console.log(selector[index].offsetTop - 800 , scroll);
            
        }

        if (scroll > selector[selector.length - 1].offsetTop - 650) {
            window.removeEventListener('scroll', function () {
                console.log('====================================');
                console.log('scroll event was removed');
                console.log('====================================');
            })
        }
    }
}

$('.content-animate').eq(0).addClass('animate');

/* initial scrolling */
// document.body.scrollTop = 70;
// document.documentElement.scrollTop = 70;

// scrolling end //