$('#uploadSlipDiv').on('click', function() {
    $('#multipartFilesDocument').trigger('click');
});

$('#multipartFilesDocument').change(function(){
    var formData = new FormData();
    formData.append('multipartFilesDocument', $('input[type=file]')[0].files[0]);
    formData.append('register.id', $("#registerId").val());

    $.ajax({
        type: "POST",
        url: "/upload-image",
        contentType: false,
        processData: false,
        data: formData,
        success: function (data) {
            refreshImage();
        }
    });
});

function refreshImage() {
    const id = $("#registerId").val();
    const url = '/refresh-image-list?id=' + id;
    $(".image-list").load(url, function() {
        $.getScript("/js/step4.js");
    });
}

function removeImage(imageId) {
    const url = '/remove-image?imageId=' + imageId + "&id=" + $("#registerId").val();
    $(".image-list").load(url, function() {
        $.getScript("/js/step4.js");
    });
}