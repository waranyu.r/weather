package th.co.runenviku.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.tags.Param;
import th.co.runenviku.controller.CoordinateConversionController;
import th.co.runenviku.models.Register;
import th.co.runenviku.payload.request.DynamicQuery;
import th.co.runenviku.payload.response.MapResponse;
import th.co.runenviku.payload.response.ParamB;
import th.co.runenviku.payload.response.RegisterResponse;
import th.co.runenviku.repository.RegisterRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class RunenvikuManagementController {

    @Autowired
    RegisterRepository registerRepository;

    @GetMapping("/")
    public ModelAndView home(@RequestParam(required=false) String id) {
        ModelAndView modelAndView = new ModelAndView("/pages/index.html");
        List<Register> register = registerRepository.findAll();

        List<String> province = new ArrayList<>();
        for(int i=0; i<register.size(); i++) {
            if(!register.contains(register.get(i).getProvince())) {
                province.add(register.get(i).getProvince());

            }
        }

        List<String> season = registerRepository.findAllGroupBySeason();
        List<String> tambons = registerRepository.findAllDistinctTambon();
        List<String> districts = registerRepository.findAllDistinctDistrict();
        List<String> provinces = registerRepository.findAllDistinctProvince();
        List<String> paramA = registerRepository.findAllDistinctParamA();
        List<String> paramB = new ArrayList<>();

        modelAndView.addObject("seasons", season);
        modelAndView.addObject("tambons", tambons);
        modelAndView.addObject("districts", districts);
        modelAndView.addObject("provinces", provinces);
        modelAndView.addObject("paramA", paramA);
        modelAndView.addObject("paramB", paramB);
        return modelAndView;
    }

    @PostMapping("/api/get-marker")
    public @ResponseBody RegisterResponse getMarker(DynamicQuery dynamicQuery) {
        if(dynamicQuery.getProvince().equalsIgnoreCase("ทั้งหมด")) dynamicQuery.setProvince(null);
        if(dynamicQuery.getDistrict().equalsIgnoreCase("ทั้งหมด")) dynamicQuery.setDistrict(null);
        if(dynamicQuery.getTambon().equalsIgnoreCase("ทั้งหมด")) dynamicQuery.setTambon(null);
        if(dynamicQuery.getParamA().equalsIgnoreCase("ทั้งหมด")) dynamicQuery.setParamA(null);
        if(dynamicQuery.getParamB().equalsIgnoreCase("ทั้งหมด")) dynamicQuery.setParamB(null);;
        if(dynamicQuery.getSeason().equalsIgnoreCase("ทั้งหมด")) dynamicQuery.setSeason(null);;

        RegisterResponse registerResponse = new RegisterResponse();

        List<Register> register = registerRepository.findAllByCriteria(dynamicQuery);

        if(null != dynamicQuery.getStandard() && "" != dynamicQuery.getStandard()) {
            for (int i = 0; i < register.size(); i++) {
                if(!isNumeric(register.get(i).getValue())) {
                    register.get(i).setValue(register.get(i).getValue().replaceAll("[^\\-.0123456789]",""));
                }

                if(dynamicQuery.getStandard().equalsIgnoreCase("gt")) {
                    if (null != register.get(i).getValue() &&
                            isNumeric(register.get(i).getValue()) &&
                            isNumeric(dynamicQuery.getStandardValue()) &&
                            Double.parseDouble(register.get(i).getValue()) < Double.parseDouble(dynamicQuery.getStandardValue())) {
                        register.remove(i);
                        i--;
                    }
                } else if(dynamicQuery.getStandard().equalsIgnoreCase("lt")) {
                    if (null != register.get(i).getValue() &&
                            isNumeric(register.get(i).getValue()) &&
                            isNumeric(dynamicQuery.getStandardValue()) &&
                            Double.parseDouble(register.get(i).getValue()) >= Double.parseDouble(dynamicQuery.getStandardValue())) {
                        register.remove(i);
                        i--;
                    }
                } else if(dynamicQuery.getStandard().equalsIgnoreCase("notBtn")) {
                    String[] arr = dynamicQuery.getStandardValue().split("-");
                    if (null != register.get(i).getValue() &&
                            isNumeric(register.get(i).getValue()) &&
                            Double.parseDouble(register.get(i).getValue()) >= Double.parseDouble(arr[0]) &&
                            Double.parseDouble(register.get(i).getValue()) <= Double.parseDouble(arr[1])) {
                        register.remove(i);
                        i--;
                    }
                } else if(dynamicQuery.getStandard().equalsIgnoreCase("btn")) {
                    String[] arr = dynamicQuery.getStandardValue().split("-");
                    if (null != register.get(i).getValue() &&
                            isNumeric(register.get(i).getValue()) &&
                            (Double.parseDouble(register.get(i).getValue()) < Double.parseDouble(arr[0]) ||
                            Double.parseDouble(register.get(i).getValue()) > Double.parseDouble(arr[1]))) {
                        register.remove(i);
                        i--;
                    }
                }
            }
        }

        CoordinateConversionController coordinateConversionController = new CoordinateConversionController();
        List<MapResponse> maps = new ArrayList<>();

        for(int i=0; i< register.size(); i++) {
            if(register.get(i).getGpsCoordinate() != null && !register.get(i).getValue().equals("-")) {
                MapResponse mapResponse = new MapResponse();
                String[] split = register.get(i).getGpsCoordinate().split(" ");

                if(split.length == 7) {
                    String utm = split[2].substring(0,2) + " " + split[2].substring(2) + " " + split[3] + " " + split[5];
                    double[] latlon = coordinateConversionController.utm2LatLon(utm);
                    mapResponse.setLat(latlon[0]);
                    mapResponse.setLon(latlon[1]);
                    mapResponse.setLocationName(register.get(i).getLocationName());
                }

                boolean isSame = false;

                MapResponse.Data data = new MapResponse.Data();
                data.setParamA(register.get(i).getParamA());
                data.setParamB(register.get(i).getParamB());
                data.setValue(register.get(i).getValue());

                if(register.get(i).getUnit().equalsIgnoreCase("false")) {
                    data.setUnit("");
                } else {
                    data.setUnit(register.get(i).getUnit());
                }
                data.setCollectedSampleDate(register.get(i).getCollectedSampleDate());

                for(int j=0; j < maps.size(); j++) {
                    if(mapResponse.getLat() == maps.get(j).getLat() &&
                        mapResponse.getLon() == maps.get(j).getLon()) {

                        maps.get(j).getDataList().add(data);
                        isSame = true;
                        break;
                    }
                }

                if(!isSame) {
                    mapResponse.getDataList().add(data);
                    maps.add(mapResponse);
                }
            }
        }

        registerResponse.setMaps(maps);

        return registerResponse;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @PostMapping("/get-district")
    @ResponseBody
    public List<String> getDistrict(String province) {
        Map<String, String> a = new HashMap<>();
        List<Register> registers = registerRepository.findDistinctByProvince(province);

        List<String> districts = new ArrayList<>();
        for(int i=0; i < registers.size(); i++) {

            boolean isSame = false;
            for(int j=0; j < districts.size(); j++) {
                if(registers.get(i).getDistrict().equalsIgnoreCase(districts.get(j))) {
                    isSame = true;
                    break;
                }
            }

            if(!isSame) {
                districts.add(registers.get(i).getDistrict());
            }
        }

        return districts;
    }

    @PostMapping("/get-tambon")
    @ResponseBody
    public List<String> getTambon(String province, String district) {
        List<Register> registers = registerRepository.findDistinctByProvinceAndDistrict(province, district);

        List<String> tambon = new ArrayList<>();
        for(int i=0; i < registers.size(); i++) {

            boolean isSame = false;
            for(int j=0; j < tambon.size(); j++) {
                if(registers.get(i).getTambon().equalsIgnoreCase(tambon.get(j))) {
                    isSame = true;
                    break;
                }
            }

            if(!isSame) {
                tambon.add(registers.get(i).getTambon());
            }
        }

        return tambon;
    }

    @PostMapping("/get-param-b")
    @ResponseBody
    public List<ParamB> getParamB(String paramA) {
        List<Register> registers = registerRepository.findAllByParamA(paramA);

        List<ParamB> paramB = new ArrayList<>();
        for(int i=0; i < registers.size(); i++) {

            boolean isSame = false;
            for(int j=0; j < paramB.size(); j++) {
                if (registers.get(i).getParamB() == null) {
                    isSame = true;
                    break;
                }

                if(registers.get(i).getParamB().equalsIgnoreCase(paramB.get(j).getParamB())) {
                    isSame = true;
                    break;
                }
            }

            if(!isSame) {
                ParamB p = new ParamB();
                String covertStandardValue = null;
                if(!registers.get(i).getStandardValue().equalsIgnoreCase("-")) {
                    covertStandardValue = registers.get(i).getStandardValue().replaceAll("[^\\-.0123456789]","");
                }

                p.setStandardValue(covertStandardValue);
                p.setParamB(registers.get(i).getParamB());
                paramB.add(p);
            }
        }

        return paramB;
    }
    @PostMapping("/get-standard")
    @ResponseBody
    public ParamB getStandard(String paramB) {
        List<Register> registers = registerRepository.findOneByParamB(paramB);

        String covertStandardValue = null;
        for(int i=0; i<registers.size(); i++) {
            if(null != registers.get(i).getStandardValue() && !"-".equalsIgnoreCase(registers.get(i).getStandardValue())) {
                covertStandardValue = registers.get(i).getStandardValue().replaceAll("[^\\-\\.0123456789]","");
            }
        }

        ParamB p = new ParamB();
        p.setStandardValue(covertStandardValue);

        return p;
    }
}