package th.co.runenviku.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import th.co.runenviku.models.Register;
import th.co.runenviku.payload.request.RegisterRequest;
import th.co.runenviku.payload.response.MessageResponse;
import th.co.runenviku.payload.response.RegisterResponse;
import th.co.runenviku.repository.RegisterRepository;
import th.co.runenviku.service.AdminService;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AdminController {
	@Autowired
	RegisterRepository registerRepository;

	@Autowired
	AdminService adminService;

	@Autowired
	FileController fileController;


	@GetMapping("/admin/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("admin/index.html");
		return modelAndView;
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public ModelAndView dashboard(@RequestParam(required = false) String p) {
		RegisterResponse registerResponse = new RegisterResponse();

		int pageNo = 1;
		double total = 0;
		int size = 10;

		if(p != null) {
			pageNo = Integer.valueOf(p);
		}

		Page<Register> pageRegisters = registerRepository.findAll(PageRequest.of(pageNo - 1, size, Sort.by("_id").descending()));
		registerResponse.setRegisters(pageRegisters.getContent());

		total = registerRepository.count();
		registerResponse.setTotal(registerRepository.count());

		List<String> imageNoSplitList = new ArrayList<>();

		int page = (int)total / size;
		if(total % size > 0) {
			page++;
		}

		registerResponse.setImagePath(imageNoSplitList);
		ModelAndView modelAndView = new ModelAndView("admin/register.html");
		modelAndView.addObject("registers", registerResponse);
		modelAndView.addObject("page", page);
		modelAndView.addObject("pageNo", pageNo);

		return modelAndView;
	}

	@RequestMapping(value = "/admin/get-more-info", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ADMIN')")
	public @ResponseBody MessageResponse getMoreInfo(@RequestBody RegisterRequest registerRequest) {
		MessageResponse messageResponse = new MessageResponse();

		try {
			messageResponse.setMessage("success");
			messageResponse.setStatus(true);
		} catch (Exception e) {
			messageResponse.setMessage("error");
			messageResponse.setStatus(false);
			e.printStackTrace();
		}

		return messageResponse;
	}

	@RequestMapping(value = "/admin/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public @ResponseBody MessageResponse save(@RequestBody RegisterRequest registerRequest) {
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setStatus(true);

		if(registerRequest.getRegister() == null) {
			messageResponse.setStatus(false);
			messageResponse.setMessage("register is null");
			return messageResponse;
		}

		try {
			if(registerRequest.getRegister().getId() == "") {
				registerRequest.getRegister().setId(null);
			}

			registerRepository.save(registerRequest.getRegister());
			messageResponse.setStatus(true);
			messageResponse.setMessage("success");
		} catch (Exception e) {
			messageResponse.setStatus(false);
			messageResponse.setMessage("failed, please try again or contact administrator");
			e.printStackTrace();
		}

		return messageResponse;
	}

	@RequestMapping(value = "/admin/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public @ResponseBody MessageResponse delete(@RequestBody RegisterRequest registerRequest) {
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setStatus(true);

		if(registerRequest.getRegister() == null && registerRequest.getRegister().getId() == null) {
			messageResponse.setStatus(false);
			messageResponse.setMessage("id is null");
			return messageResponse;
		}

		try {
			registerRepository.delete(registerRequest.getRegister());
			messageResponse.setStatus(true);
			messageResponse.setMessage("success");
		} catch (Exception e) {
			messageResponse.setStatus(false);
			messageResponse.setMessage("failed, please try again or contact administrator");
			e.printStackTrace();
		}

		return messageResponse;
	}
}