package th.co.runenviku.payload.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DynamicQuery {
	private String province;
	private String district;
	private String tambon;
	private String paramA;
	private String paramB;
	private String value;
	private String season;
	private String standard;
	private String standardValue;
}
