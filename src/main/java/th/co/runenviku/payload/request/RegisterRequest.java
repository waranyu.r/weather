package th.co.runenviku.payload.request;

import org.springframework.web.multipart.MultipartFile;
import th.co.runenviku.models.Register;

import javax.validation.constraints.NotBlank;

public class RegisterRequest {
	@NotBlank
	private Register register;

	private MultipartFile multipartFilesDocument;

	public Register getRegister() {
		return register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}

	public MultipartFile getMultipartFilesDocument() {
		return multipartFilesDocument;
	}

	public void setMultipartFilesDocument(MultipartFile multipartFilesDocument) {
		this.multipartFilesDocument = multipartFilesDocument;
	}
}
