package th.co.runenviku.payload.response;

import lombok.Getter;
import lombok.Setter;
import th.co.runenviku.models.Register;

import java.util.List;

@Getter
@Setter
public class RegisterResponse {
	private List<Register> registers;
	private List<String> imagePath;
	private long total;

	private List<MapResponse> maps;
}
