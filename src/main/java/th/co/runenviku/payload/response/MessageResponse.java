package th.co.runenviku.payload.response;

public class MessageResponse {
	private String message;
	private boolean status;

	public MessageResponse(String message) {
		this.message = message;
	}

	public MessageResponse() {

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
