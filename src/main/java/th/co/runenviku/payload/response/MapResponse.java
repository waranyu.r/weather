package th.co.runenviku.payload.response;

import lombok.Getter;
import lombok.Setter;
import th.co.runenviku.models.Register;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class MapResponse {
	String locationName;
	private double lat;
	private double lon;

	private List<Data> dataList = new ArrayList<>();

	@Getter
	@Setter
	public static class Data {
		private String paramA;
		private String paramB;
		private String value;
		private String unit;
		private String collectedSampleDate;
	}
}
