package th.co.runenviku.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ParamB {
	private String standardValue;
	private String paramB;
}
