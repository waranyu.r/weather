package th.co.runenviku.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import th.co.runenviku.models.Register;

import java.util.List;

public interface RegisterRepository extends MongoRepository<Register, String>, RegisterRepositoryCustom {
    Register findRegisterById(String id);
    Page<Register> findAll(Pageable pageable);

    List<Register> findDistinctByProvince(String province);
    List<Register> findDistinctByProvinceAndDistrict(String province, String district);
    List<Register> findAllByProvinceAndDistrictAndTambon(String province, String district, String tambon);
    List<Register> findAllByProvinceAndDistrict(String province, String district);
    List<Register> findAllByProvinceAndParamAAndParamB(String province, String paramA, String paramB);
    List<Register> findAllByParamA(String paramA);
    List<Register> findOneByParamB(String paramB);

    long count();
}