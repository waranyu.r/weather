package th.co.runenviku.repository.dao;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import th.co.runenviku.models.Register;
import th.co.runenviku.payload.request.DynamicQuery;
import th.co.runenviku.repository.RegisterRepositoryCustom;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RegisterRepositoryImpl implements RegisterRepositoryCustom {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<String> findAllGroupBySeason() {
        List<String> seasons = new ArrayList<>();
        MongoCollection mongoCollection = mongoTemplate.getCollection("register");
        DistinctIterable distinctIterable = mongoCollection.distinct("season", String.class);
        MongoCursor cursor = distinctIterable.iterator();
        while (cursor.hasNext()) {
            String season = (String)cursor.next();
            seasons.add(season);
        }

        return seasons;
    }

    @Override
    public List<String> findAllDistinctDistrict() {
        List<String> seasons = new ArrayList<>();
        MongoCollection mongoCollection = mongoTemplate.getCollection("register");
        DistinctIterable distinctIterable = mongoCollection.distinct("district", String.class);
        MongoCursor cursor = distinctIterable.iterator();
        while (cursor.hasNext()) {
            String season = (String)cursor.next();
            seasons.add(season);
        }

        return seasons;
    }

    @Override
    public List<String> findAllDistinctProvince() {
        List<String> seasons = new ArrayList<>();
        MongoCollection mongoCollection = mongoTemplate.getCollection("register");
        DistinctIterable distinctIterable = mongoCollection.distinct("province", String.class);
        MongoCursor cursor = distinctIterable.iterator();
        while (cursor.hasNext()) {
            String season = (String)cursor.next();
            seasons.add(season);
        }

        return seasons;
    }

    @Override
    public List<String> findAllDistinctTambon() {
        List<String> seasons = new ArrayList<>();
        MongoCollection mongoCollection = mongoTemplate.getCollection("register");
        DistinctIterable distinctIterable = mongoCollection.distinct("tambon", String.class);
        MongoCursor cursor = distinctIterable.iterator();
        while (cursor.hasNext()) {
            String season = (String)cursor.next();
            seasons.add(season);
        }

        return seasons;
    }

    @Override
    public List<String> findAllDistinctParamA() {
        List<String> seasons = new ArrayList<>();
        MongoCollection mongoCollection = mongoTemplate.getCollection("register");
        DistinctIterable distinctIterable = mongoCollection.distinct("paramA", String.class);
        MongoCursor cursor = distinctIterable.iterator();
        while (cursor.hasNext()) {
            String season = (String)cursor.next();
            seasons.add(season);
        }

        return seasons;
    }

    @Override
    public List<String> findAllDistinctParamB() {
        List<String> seasons = new ArrayList<>();
        MongoCollection mongoCollection = mongoTemplate.getCollection("register");
        DistinctIterable distinctIterable = mongoCollection.distinct("paramB", String.class);
        MongoCursor cursor = distinctIterable.iterator();
        while (cursor.hasNext()) {
            String season = (String)cursor.next();
            seasons.add(season);
        }

        return seasons;
    }

    @Override
    public List<Register> findAllByCriteria(DynamicQuery dynamicQuery) {
        final Query query = new Query();
        if(dynamicQuery.getProvince() != null) {
            query.addCriteria(Criteria.where("province").is(dynamicQuery.getProvince()));
        }
        if(dynamicQuery.getDistrict() != null) {
            query.addCriteria(Criteria.where("district").is(dynamicQuery.getDistrict()));
        }
        if(dynamicQuery.getTambon() != null) {
            query.addCriteria(Criteria.where("tambon").is(dynamicQuery.getTambon()));
        }
        if(dynamicQuery.getParamA() != null) {
            query.addCriteria(Criteria.where("paramA").is(dynamicQuery.getParamA()));
        }
        if(dynamicQuery.getParamB() != null) {
            query.addCriteria(Criteria.where("paramB").is(dynamicQuery.getParamB()));
        }
        if(dynamicQuery.getSeason() != null) {
            query.addCriteria(Criteria.where("season").is(dynamicQuery.getSeason()));
        }

        query.with(Sort.by(Sort.Direction.ASC, "locationName"));

        return mongoTemplate.find(query, Register.class);
    }
}