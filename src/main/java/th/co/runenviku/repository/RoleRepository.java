package th.co.runenviku.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import th.co.runenviku.models.Role;
import th.co.runenviku.models.enums.ERole;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
	Optional<Role> findByName(ERole name);
}
