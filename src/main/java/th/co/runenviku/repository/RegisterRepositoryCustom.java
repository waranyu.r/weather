package th.co.runenviku.repository;

import th.co.runenviku.models.Register;
import th.co.runenviku.payload.request.DynamicQuery;

import java.util.List;

public interface RegisterRepositoryCustom {

    List<String> findAllGroupBySeason();

    List<String> findAllDistinctTambon();

    List<String> findAllDistinctParamA();

    List<String> findAllDistinctParamB();

    List<String> findAllDistinctDistrict();

    List<String> findAllDistinctProvince();

    List<Register> findAllByCriteria(DynamicQuery dynamicQuery);
}
