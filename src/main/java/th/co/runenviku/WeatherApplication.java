package th.co.runenviku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import th.co.runenviku.repository.FileStorageRepository;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageRepository.class
})
public class WeatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherApplication.class, args);
	}

}
