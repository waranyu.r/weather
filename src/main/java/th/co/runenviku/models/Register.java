package th.co.runenviku.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "register")
@Getter
@Setter
public class Register {
    @Id
    private String id;

    private String locationName;

    private String gpsCoordinate;

    private String address;

    private String province;

    private String district;

    private String tambon;

    private String season;

    private String collectedSampleDate;

    private String paramA;

    private String paramB;

    private String unit;

    private String value;

    private String standardValue;

    private String type;

}