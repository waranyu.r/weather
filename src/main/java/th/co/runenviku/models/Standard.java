package th.co.runenviku.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "standard")
public class Standard {
    @Id
    private String id;

    private String parameter;

    private String unit;

    private String value;

    private String valuePowerBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValuePowerBy() {
        return valuePowerBy;
    }

    public void setValuePowerBy(String valuePowerBy) {
        this.valuePowerBy = valuePowerBy;
    }
}