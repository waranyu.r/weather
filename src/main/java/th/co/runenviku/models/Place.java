package th.co.runenviku.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "place")
public class Place {
    @Id
    private String id;

    private String provinceThai;

    private String districtThaiShort;

    private String tambonThaiShort;

    private String postCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvinceThai() {
        return provinceThai;
    }

    public void setProvinceThai(String provinceThai) {
        this.provinceThai = provinceThai;
    }

    public String getDistrictThaiShort() {
        return districtThaiShort;
    }

    public void setDistrictThaiShort(String districtThaiShort) {
        this.districtThaiShort = districtThaiShort;
    }

    public String getTambonThaiShort() {
        return tambonThaiShort;
    }

    public void setTambonThaiShort(String tambonThaiShort) {
        this.tambonThaiShort = tambonThaiShort;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
