package th.co.runenviku.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import th.co.runenviku.repository.RegisterRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {

	@Autowired
	RegisterRepository registerRepository;

	public String[] removeStringnNull(String[] arr) {
		List<String> values = new ArrayList<String>();
		for(String data: arr) {
			if(data != null && data != "") {
				values.add(data);
			}
		}

		return values.toArray(new String[values.size()]);
	}
}